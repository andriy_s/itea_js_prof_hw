/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/Observer/CustomEvents.js":
/*!**********************************************!*\
  !*** ./application/Observer/CustomEvents.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n\r\nconst CustomEvents = () => {\r\n\r\n  /*\r\n    JS позволяет нам создавать собственные события.\r\n    Синтаксис:\r\n      Обязательное только имя события:\r\n      new Event('eventName', {bubbles: true, cancelable: false});\r\n\r\n      Кастом ивент еще имеет доп. поле \"detail\", через которое можно\r\n      передать данные:\r\n      new CustomEvent(\"userLogin\", { detail: {...} });\r\n\r\n  */\r\n  let itea_event = new Event('build');\r\n  let itea_customEvent = new CustomEvent(\"userLogin\", {\r\n    detail: {\r\n      username: \"davidwalsh\"\r\n    }\r\n  });\r\n\r\n  // Абстрактный пример:\r\n  let myElement = document.createElement('button');\r\n      myElement.innerText = 'Custom Event!';\r\n      myElement.addEventListener(\"userLogin\", function(e) {\r\n        console.info(\"Event is: \", e);\r\n        console.info(\"Custom data is: \", e.detail);\r\n        myElement.style.backgroundColor = 'blue';\r\n      });\r\n\r\n      myElement.addEventListener('click', function(){\r\n        myElement.dispatchEvent(itea_customEvent);\r\n      });\r\n      document.body.appendChild(myElement);\r\n\r\n  // Реальный пример:\r\n\r\n  let message__container = document.getElementById('message__container');\r\n  let messageAuthor = document.getElementById('messageAuthor');\r\n  let messageText = document.getElementById('messageText');\r\n  let messageSendBtn = document.getElementById('messageSendBtn');\r\n\r\n      messageSendBtn.addEventListener('click', function(){\r\n        let author = messageAuthor.value;\r\n        let text = messageText.value;\r\n        let MessageSendEvent = new CustomEvent(\"messageSend\", {\r\n          detail: {\r\n            author: author,\r\n            message: text\r\n          }\r\n        });\r\n        message__container.dispatchEvent(MessageSendEvent);\r\n      });\r\n\r\n      message__container.addEventListener('messageSend', function(event){\r\n        let { author, message } = event.detail;\r\n        let messageNode = document.createElement('div');\r\n            messageNode.innerHTML = `<div class=\"message\"><b>${author}:</b> ${message}</div>`;\r\n        message__container.appendChild(messageNode);\r\n      });\r\n\r\n\r\n\r\n}; // custom events end!\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (CustomEvents);\r\n\n\n//# sourceURL=webpack:///./application/Observer/CustomEvents.js?");

/***/ }),

/***/ "./application/Observer/Observer.js":
/*!******************************************!*\
  !*** ./application/Observer/Observer.js ***!
  \******************************************/
/*! exports provided: Observable, Observer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Observable\", function() { return Observable; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Observer\", function() { return Observer; });\nfunction Observable(){\r\n  // Создаем список подписаных обьектов\r\n  var observers = [];\r\n  // Оповещение всех подписчиков о сообщении\r\n  this.sendMessage = function( msg ){\r\n      observers.map( ( obs ) => {\r\n        obs.notify(msg);\r\n      });\r\n  };\r\n  // Добавим наблюдателя\r\n  this.addObserver = function( observer ){\r\n    observers.push( observer );\r\n  };\r\n}\r\n\r\n// Сам наблюдатель:\r\nfunction Observer( behavior ){\r\n  // Делаем функцию, что бы через callback можно\r\n  // было использовать различные функции внутри\r\n  this.notify = function( msg ){\r\n    behavior( msg );\r\n  };\r\n}\r\n\r\n\r\n\n\n//# sourceURL=webpack:///./application/Observer/Observer.js?");

/***/ }),

/***/ "./application/Observer/demo1.js":
/*!***************************************!*\
  !*** ./application/Observer/demo1.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Observer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Observer */ \"./application/Observer/Observer.js\");\n\r\n\r\nconst Demo1 = () => {\r\n\r\n  let observable = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observable\"]();\r\n  let obs1 = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( (msg) => console.log(msg));\r\n  let obs2 = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( (msg) => console.warn(msg));\r\n  let obs3 = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( (msg) => console.error(msg));\r\n\r\n      observable.addObserver( obs1 );\r\n      observable.addObserver( obs2 );\r\n      observable.addObserver( obs3 );\r\n\r\n  console.log( observable );\r\n\r\n  //  Проверим абстрактно как оно работает:\r\n  setTimeout(\r\n    ()=>{\r\n      // оправим сообщение, с текущей датой:\r\n      observable.sendMessage('Now is' + new Date());\r\n    }, 2000\r\n  );\r\n\r\n\r\n};\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Demo1);\r\n\n\n//# sourceURL=webpack:///./application/Observer/demo1.js?");

/***/ }),

/***/ "./application/Observer/demo2.js":
/*!***************************************!*\
  !*** ./application/Observer/demo2.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Observer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Observer */ \"./application/Observer/Observer.js\");\n\r\n\r\nconst Demo2 = () => {\r\n\r\n  /*\r\n    Рассмотрим на примере интернет магазина:\r\n  */\r\n\r\n  let Products = [\r\n    {\r\n      id: 1,\r\n      name: 'Samsung Galaxy S8 ',\r\n      price: 21999,\r\n      imageLink: 'https://i1.rozetka.ua/goods/1894533/samsung_galaxy_s8_64gb_black_images_1894533385.jpg'\r\n    },\r\n    {\r\n      id: 2,\r\n      name: 'Apple AirPort Capsule',\r\n      price: 10700,\r\n      imageLink: 'https://i1.rozetka.ua/goods/3330569/apple_a1470_me177_images_3330569615.jpg'\r\n    },\r\n    {\r\n      id: 3,\r\n      name: 'Apple iPhone X',\r\n      price: 35999,\r\n      imageLink: 'https://i1.rozetka.ua/goods/2433231/apple_iphone_x_64gb_silver_images_2433231297.jpg'\r\n    },\r\n    {\r\n      id: 4,\r\n      name: 'LG G6 Black ',\r\n      price: 15999,\r\n      imageLink: 'https://i1.rozetka.ua/goods/1892329/copy_lg_lgh845_acistn_58d8fc4a87d51_images_1892329834.jpg'\r\n    }\r\n  ];\r\n\r\n  // Создадим наблюдателя:\r\n  let observable = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observable\"]();\r\n  // Трех обсерверов:\r\n  let basketObs = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( function(id){\r\n    let filtredToBasket = Products.filter( item => Number(item.id) === Number(id) );\r\n        // console.log();\r\n        Cart.push( filtredToBasket[0] );\r\n        renderBasket();\r\n  });\r\n\r\n  let serverObs = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( (id) => {\r\n      let filtredToBasket = Products.filter( item => Number(item.id) === Number(id) );\r\n      let msg = `Товар ${filtredToBasket[0].name} добавлен в корзину`;\r\n      console.log( msg );\r\n  });\r\n\r\n  let iconObs = new _Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( (id) => {\r\n      let filtredToBasket = Products.filter( item => Number(item.id) === Number(id) );\r\n\r\n      let products__cart = document.getElementById('products__cart');\r\n          products__cart.innerText = Cart.length;\r\n  });\r\n\r\n  observable.addObserver( basketObs );\r\n  observable.addObserver( serverObs );\r\n  observable.addObserver( iconObs );\r\n\r\n  // Render Data - - - - - - - - - - - -  \r\n  let Cart = [];\r\n  let products__row = document.getElementById('products__row');\r\n\r\n  function renderBasket(){\r\n    let cartElem = document.getElementById('cart');\r\n    let message;\r\n        if( Cart.length === 0 ){\r\n          message = 'У вас в корзине пусто';\r\n        } else {\r\n          let Sum = Cart.reduce( (prev, current) => {\r\n            return prev += Number(current.price);\r\n          }, 0);\r\n          message = `У вас в корзине ${Cart.length} товаров, на сумму: ${Sum} грн.`;\r\n        }\r\n        cartElem.innerHTML = `<h2>${message}</h2><ol></ol>`;\r\n\r\n        let ol = cartElem.querySelector('ol');\r\n        Cart.map( item => {\r\n          let li = document.createElement('li');\r\n              li.innerText = `${item.name} (${item.price} грн.)`;\r\n              ol.appendChild(li);\r\n        });\r\n  }\r\n\r\n\r\n  Products.map( item => {\r\n    let product = document.createElement('div');\r\n        product.className = \"product\";\r\n        product.innerHTML =\r\n        `<div class=\"product__image\">\r\n            <img src=\"${item.imageLink}\"/>\r\n          </div>\r\n          <div class=\"product__name\">${item.name}</div>\r\n          <div class=\"product__price\">${item.price} грн.</div>\r\n          <div class=\"product__action\">\r\n            <button class=\"product__buy\" data-id=${item.id}> Купить </button>\r\n          </div>`;\r\n        let buyButton = product.querySelector('.product__buy');\r\n            buyButton.addEventListener('click', (e) => {\r\n              let id = e.target.dataset.id;\r\n              observable.sendMessage(id);\r\n            });\r\n        products__row.appendChild(product);\r\n  });\r\n\r\n  renderBasket();\r\n};\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Demo2);\r\n\n\n//# sourceURL=webpack:///./application/Observer/demo2.js?");

/***/ }),

/***/ "./application/Observer/index.js":
/*!***************************************!*\
  !*** ./application/Observer/index.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _demo1__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./demo1 */ \"./application/Observer/demo1.js\");\n/* harmony import */ var _demo2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./demo2 */ \"./application/Observer/demo2.js\");\n\r\n\r\n\r\nconst ObserverDemo = () => {\r\n\r\n  // Abstract Demo\r\n  // Demo1();\r\n\r\n  // Functional Demo:\r\n  Object(_demo2__WEBPACK_IMPORTED_MODULE_1__[\"default\"])();\r\n\r\n\r\n\r\n\r\n\r\n}; //observer Demo\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (ObserverDemo);\r\n\n\n//# sourceURL=webpack:///./application/Observer/index.js?");

/***/ }),

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Observer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Observer */ \"./application/Observer/index.js\");\n/* harmony import */ var _Observer_CustomEvents__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Observer/CustomEvents */ \"./application/Observer/CustomEvents.js\");\n// Точка входа в наше приложение\r\n\r\n\r\n\r\n\r\n// 1. Observer ->\r\nObject(_Observer__WEBPACK_IMPORTED_MODULE_0__[\"default\"])();\r\n// 2. CustomEvents ->\r\n// CustomEvents();\r\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ })

/******/ });
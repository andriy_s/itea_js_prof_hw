/*
  Задание:

    I. Написать класс Post который будет уметь:

      1. Конструктор:
          title
          image
          description
          likes

      2. Методы
          render -> отрисовать элемент в ленте
          likePost -> увеличивает счетчик постов
          Методы для изменения title, image, description
          + бонус. Сделать получение и изменение через set и get

    II. Написать класс Advertisment который будет экстендится от класа Post
        но помимо будет иметь другой шаблон вывода, а так же метод для покупки обьекта

        buyItem -> выведет сообщение что вы купили обьект

    III.  Сгенерировать ленту из всех постов что вы добавили в систему.
          Каждый третий пост должен быть рекламным
*/
  let feed = [];
  class Post {

    constructor( title, image, description, likes ){
      this.title = title;
      this.image = image;
      this.description = description;
      this.likes = likes;

      feed.push(this);
    }

    render() {
      console.log('this', this);
      let x =document.createElement('div');
      let exampleCreate = `<div>${this.title}</div>
                            <div><img src = "${this.image}"</div>
                            <div>${this.description}</div>
                            <button id="zButton">Likes: ${(this.likes)}</button>`;
      document.getElementById('zButton').addEventListener("click", () => {
        this.likes++;
        `<button id="zButton">Likes: ${(this.likes)}</button>`
      })
      x.innerHTML = exampleCreate;
      document.body.appendChild(x); 
    }

  }

  let addPost = new Post("ABC", "images/cat1.jpg", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", 0);
      addPost.render();
     
  // console.log(addPost);
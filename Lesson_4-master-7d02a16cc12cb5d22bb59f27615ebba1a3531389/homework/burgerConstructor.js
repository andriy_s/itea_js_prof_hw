/*

  Задание:

    1. Создать конструктор бургеров на прототипах, которая добавляет наш бургер в массив меню

    Дожно выглядеть так:
    new burger('Hamburger',[ ...Массив с ингредиентами ] , 20);

    Прототип для бургера:
    {
      cookingTime: 0,     // Время на готовку
      showComposition: function(){
        let {composition, name} = this;
        let compositionLength = composition.length;
        console.log(compositionLength);
        if( compositionLength !== 0){
          composition.map( function( item ){
              console.log( 'Состав бургера', name, item );
          })
        }
      }
    }

    Результатом конструктора нужно вывести массив меню c добавленными элементами.
    // menu: [ {name: "", composition: [], cookingTime:""},  {name: "", composition: [], cookingTime:""}]

      2. Создать конструктор заказов

      Прототип:
      {
        id: "",
        orderNumber: "",
        orderBurder: "",
        orderException: "",
        orderAvailability: ""
      }

        Заказ может быть 3х типов:
          1. В котором указано название бургера
            new Order('Hamburger'); -> Order 1. Бургер Hamburger, будет готов через 10 минут.
          2. В котором указано что должно быть в бургере, ищете в массиве Menu подходящий вариант
            new Order('', 'has', 'Название ингредиента') -> Order 2. Бургер Чизбургер, с сыром, будет готов через 5 минут.
          3. В котором указано чего не должно быть
            new Order('', 'except', 'Название ингредиента') ->


          Каждый их которых должен вернуть статус:
          "Заказ 1: Бургер ${Название}, будет готов через ${Время}

          Если бургера по условиям заказа не найдено предлагать случайный бургер из меню:
            new Order('', 'except', 'Булка') -> К сожалению, такого бургера у нас нет, можете попробовать "Чизбургер"
            Можно например спрашивать через Confirm подходит ли такой вариант, если да - оформлять заказ
            Если нет, предложить еще вариант из меню.

      3. Протестировать программу.
        1. Вначале добавляем наши бургеры в меню (3-4 шт);
        2. Проверяем работу прототипного наследования вызывая метод showComposition на обьект бургера с меню
        3. Формируем 3 заказа

      Бонусные задания:
      4. Добавлять в исключения\пожелания можно несколько ингредиентов
      5. MEGABONUS
        Сделать графическую оболочку для программы.

*/

const Ingredients = [
    'Булка',
    'Огурчик',
    'Котлетка',
    'Бекон',
    'Рыбная котлета',
    'Соус карри',
    'Кисло-сладкий соус',
    'Помидорка',
    'Маслины',
    'Острый перец',
    'Капуста',
    'Кунжут',
    'Сыр Чеддер',
    'Сыр Виолла',
    'Сыр Гауда',
    'Майонез'
];

//  var OurMenu = [];
//  var OurOrders = [];

const IdGenerator = {};
IdGenerator.value = 0;
IdGenerator.nextValue = function () {
    return ++IdGenerator.value;
};

const randomBetween = (min, max) => Math.floor(Math.random() * (max - min) + min);


const Burger = function (name, ingredients, cookingTime) {
    this.name = name;
    this.composition = ingredients || [];
    this.cookingTime = cookingTime || 0;
};

Burger.prototype = {
    showComposition: function () {
        let {composition, name, cookingTime} = this;
        let compositionLength = composition.length;

        console.log(compositionLength);
        if (compositionLength !== 0) {
            composition.map(function (item) {
                console.log('Состав бургера', name, item);
            })
        }
    }
};


const Menu = (function () {

    let instance;

    function Menu() {
        if (instance) {
            return instance;
        }
        this.items = [];
        instance = this;
    }

    Menu.prototype = {
        addBurger(burger) {
            this.items.push(burger);
        },

        getAll() {
            return this.items;
        },

        showAll() {
            this.items.forEach(burger =>
                console.log(`Бургер. name: ${burger.name}, composition: ${burger.composition}, cookingTime: ${burger.cookingTime} мин.`));
        },

        findByIndex(index) {
            if (index > this.items.length) {
                return undefined;
            }
            return this.items[index];
        },

        findBurgerByName(name) {
            let result;
            this.items.forEach(burger => {
                if (burger.name === name) {
                    result = burger;
                }
            });
            return result;
        },

        findBurgerByIngredients(ingredients, condition) {
            let result;

            this.items.forEach(burger => {
                let found = true;
                ingredients.forEach(ingredient => {
                    if (burger.composition.indexOf(ingredient) === -1 && condition === "has" ||
                        burger.composition.indexOf(ingredient) !== -1 && condition === "except") {
                        found = false;
                    }
                });
                if (found) {
                    result = burger;
                }
            });

            return result;
        }

    };

    return Menu;

})();


const Order = function (name, condition, value) {
    const id = IdGenerator.nextValue(),
        menu = new Menu(),
        printOrder = (burger) =>
            console.log(`Заказ ${this.orderNumber}: Бургер ${burger.name}, будет готов через ${burger.cookingTime} мин`);

    this.id = id;
    this.orderNumber = id;
    this.orderException = [];
    this.orderAvailability = [];
    if (condition === "except") {
        this.orderException.push(value);
    }
    if (condition === "has") {
        this.orderAvailability.push(value);
    }

    let burger;
    if (!!name) {

        burger = menu.findBurgerByName(name);
        if (!burger) {
            throw new Exception(`Не найден бургер с указанным названием: ${name}`);
        }
        this.orderBurger = burger;
    } 
    else {

        if (["has", "except"].indexOf(condition) === -1) {
            throw new Exception(`Неверно указано условие: ${condition} Значение должно = ["has", "except"]`);
        }
        burger = menu.findBurgerByIngredients(value, condition);
    }

    if (!!burger) {

        printOrder(burger);
    } else {

        let counter = 0;

        while (counter < 3) {
            let index = randomBetween(0, menu.getAll().length);
            burger = menu.findByIndex(index);

            if (confirm(`К сожалению, такого бургера у нас нет, можете попробовать ${burger.name}`)) {
                printOrder(burger);
                break;
            }
            ++counter;
        }
    }

};


let cheesburger = new Burger("Cheesburger",
    [Ingredients[0], Ingredients[1], Ingredients[11], Ingredients[12], Ingredients[13], Ingredients[15]],
    3);

let hamburger = new Burger("Hamburger",
    [Ingredients[0], Ingredients[1], Ingredients[3], Ingredients[5], Ingredients[7], Ingredients[12]],
    5);

let fishmac = new Burger("FishMac",
    [Ingredients[0], Ingredients[1], Ingredients[4], Ingredients[7], Ingredients[11], Ingredients[14], Ingredients[15]],
    4);

let menu = new Menu();

menu.addBurger(cheesburger);
menu.addBurger(hamburger);
menu.addBurger(fishmac);

menu.getAll().forEach(burger => burger.showComposition());
menu.showAll();


let order1 = new Order("Cheesburger");

let order2 = new Order("", "has", [Ingredients[0], Ingredients[1], Ingredients[14]]);

let order3 = new Order("", "except", [Ingredients[11], Ingredients[15]]);

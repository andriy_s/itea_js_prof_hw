/*
  PROMISES
*/

  // PROMISES
  // Promise status : Pending | Fulfilled | Rejected

  // function LogMessage( text ){
  //   let modifiedText = "Mod " + text;
  //   console.log('text:', text, modifiedText);
  //   return modifiedText;
  // }
  // let TestPromise = new Promise( (resolve, reject) => {
  //   setTimeout( () => {
  //     // переведёт промис в состояние fulfilled с результатом "result"
  //     // resolve("result");
  //     reject(false);
  //   }, 1000);
  // });
  // TestPromise.then(
  //   res => {
  //     console.log('Fulfilled: ' + res );
  //     return 'Hello World';
  //   },
  //   err => {
  //     new Error( err );
  //     return 'Sorry, data not recived :(';
  //   }
  // )
  // .then(LogMessage)
  // .then(
  //   response => {
  //     console.log('Chain 3',response);
  //   }
  // );

  /* ASYNC PROMISE
  */
  // let poop = ( res ) => console.log( res );
  /* LOAD CAT PROMISE */
  //
  function loadImagePromise( url ){
    return new Promise( (resolve, reject) => {
      let imageElement = new Image();
          imageElement.onload = function(){
            resolve( imageElement );
          };
          imageElement.onerror = function(){
            let message = 'Error on image load at url ' + url;
            new Error(message);
            reject(
              RenderImage('images/cat5.jpg')
            );
          };
          imageElement.src = url;
    });
  }
  // loadImagePromise('asdasd');
  // loadImagePromise('images/cat1.jpg')
  // .then((img) => {
  //   console.log( img );
  //   RenderImage(img.src);
  // });

  Promise.all([
    loadImagePromise('images/cat1.jpg'),
    loadImagePromise('images/cat2.jpg'),
    loadImagePromise('images/cat3.jpg'),
    loadImagePromise('images/cat4.jpg'),
    loadImagePromise('images/cat5.jpg'),
    loadImagePromise('images/cat6.jpg'),
    loadImagePromise('images/cat7.jpg')
  ])
  // .then( images => {
  //   console.log(images);
  //   // let x = [{ src: 'images/callback_hell.jpg'}];
  //   // return images;
  // });
  .then( images => {
      console.log( images );
      images.forEach( img => RenderImage( img.src ));
  });

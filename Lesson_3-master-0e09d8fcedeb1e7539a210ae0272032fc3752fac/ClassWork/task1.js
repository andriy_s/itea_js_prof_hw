/*
  Задача:

  1. При помощи fetch получить данные:
     http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2

  2. Полученый ответ преобразовать в json вызвав метод .json с объекта ответа
  3. Выбрать случайного человека и передать в следующий чейн промиса
  4. Сделать дополнительный запрос на получение списка друзей человека
     http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2
     И дальше передать обьект:
      {
        name: userName,
        ...
        friends: friendsData
      }


     Подсказка нужно вызвать дополнительный fecth из текущего чейна.
     Для того что бы передать результат выполнения доп. запроса
     в наш первый промис используйте констуркцию:

      .then(
        response1 => {
          return fetch(...)
            .then(
              response2 => {
                ...
                формируете обьект в котором будут данные человека с
                первого запроса, например его name response1.name
                и друзья которые пришли из доп. запроса
              }
            )
        }
      )

  5. Вывести результат на страничку

  + Бонус. Для конвертации обьекта response в json использовать одну
    функцию.


*/

var x = fetch("http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2")
  .then( res => {
    return res.json();
  }).then( res => {
     // console.log(res);
    let count = randomInteger(0, res.length - 1);
    return res[count];
  })
  .then(ChoosePerson)

  .then( response1 => {
    return fetch("http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2")
      .then( res => res.json() )
      .then(
       response2 => {
        let obj = {
          name: response1.name,
          age: response1.age,
          index: response1.index,
          eyeColor: response1.eyeColor,
          favoriteFruit: response1.favoriteFruit,
          friends: response2[0].friends
        };
        console.log(response1, response2);
        console.log(obj);
      })
  })


function randomInteger(min, max) {
    let rand = min + Math.random() * (max + 1 - min);
    rand = Math.floor(rand);
    return rand;
  }

function ChoosePerson (item) { 
  console.log(item);
  return item;
}

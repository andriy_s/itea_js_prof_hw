/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/facade/api.js":
/*!***********************************!*\
  !*** ./application/facade/api.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst api = {\r\n  fethchedData: null,\r\n  status: {\r\n    fetched: false,\r\n    fetching: true\r\n  },\r\n  fetchData: () => {\r\n    let url = 'https://next.json-generator.com/api/json/get/V1st09BFE';\r\n\r\n    api.status.fetched = false;\r\n    api.status.fetching = true;\r\n\r\n    const returnGlobalData = () => api.fethchedData;\r\n    return fetch(url)\r\n      .then( res => res.json() )\r\n      .then( res => {\r\n        api.status.fetched = true;\r\n        api.status.fetching = false;\r\n        api.fethchedData = res;\r\n        console.log(api);\r\n        return res;\r\n      });\r\n  },\r\n  getList: () => {\r\n    if( api.fethchedData !== null){\r\n      api.fethchedData.map( item => console.log(item.company) );\r\n    } else {\r\n      console.error('Data not fetched');\r\n    }\r\n  },\r\n  getFetchStatus: () => {\r\n    return api.status;\r\n  },\r\n  getUser: id => {\r\n    return api.fethchedData[id];\r\n  }\r\n\r\n\r\n\r\n\r\n\r\n};\r\n\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (api);\r\n\n\n//# sourceURL=webpack:///./application/facade/api.js?");

/***/ }),

/***/ "./application/facade/index.js":
/*!*************************************!*\
  !*** ./application/facade/index.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./api */ \"./application/facade/api.js\");\n\r\n\r\nconst FacadeDemo = () => {\r\n\r\n  /*\r\n    Фасад — это структурный паттерн проектирования,\r\n    который предоставляет простой интерфейс\r\n    к сложной системе классов, библиотеке или фреймворку.\r\n\r\n    https://refactoring.guru/ru/design-patterns/facade\r\n  */\r\n  const myFunc = ( data ) => {\r\n    console.log( 'myFunc', data );\r\n    _api__WEBPACK_IMPORTED_MODULE_0__[\"default\"].getList();\r\n    console.log(\r\n       'fetch status', _api__WEBPACK_IMPORTED_MODULE_0__[\"default\"].getFetchStatus(),\r\n       'user', _api__WEBPACK_IMPORTED_MODULE_0__[\"default\"].getUser(0)\r\n      );\r\n  };\r\n\r\n  _api__WEBPACK_IMPORTED_MODULE_0__[\"default\"].fetchData().then( myFunc );\r\n};\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (FacadeDemo);\r\n\n\n//# sourceURL=webpack:///./application/facade/index.js?");

/***/ }),

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _singleton___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./singleton/ */ \"./application/singleton/index.js\");\n/* harmony import */ var _facade__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./facade */ \"./application/facade/index.js\");\n// Точка входа в наше приложение\r\n\r\n\r\n\r\n/*\r\n  Обратите внимание что в среде разработки Singleton / singleton\r\n  это один файл и ошибки из-за реестра не будет, но в продакшене\r\n  это может дать ошибку, потому что например на CentOS такие импотры\r\n  уже не отрабатывают и еррорят.\r\n\r\n  + в файлах можно не указывать .js\r\n  + можно подключать файлы с названием index.js обращаясь напрямую к папке\r\n  напрмер если нужно подключить файл ./singleton/index.js,\r\n  то можно зададь в url просто ./singleton\r\n*/\r\n\r\n/*\r\n  Партерны можно поделить на три большие группы:\r\n  - Порождающие\r\n  - Структурные\r\n  - Поведенческие\r\n\r\n*/\r\n\r\n  // Singleton();\r\n  Object(_facade__WEBPACK_IMPORTED_MODULE_1__[\"default\"])();\r\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./application/singleton/index.js":
/*!****************************************!*\
  !*** ./application/singleton/index.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _object_freeze__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./object_freeze */ \"./application/singleton/object_freeze.js\");\n/* harmony import */ var _old_singleton__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./old-singleton */ \"./application/singleton/old-singleton.js\");\n/* harmony import */ var _new_singleton__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./new-singleton */ \"./application/singleton/new-singleton.js\");\n\r\n\r\n\r\n// FreezeDemo();\r\n\r\nconst SingletonDemo = () => {\r\n  /*\r\n    Singleton (он же одиночка)— это паттерн проектирования,\r\n    который гарантирует, что у класса есть только один экземпляр,\r\n    и предоставляет к нему глобальную точку доступа.\r\n\r\n    Про паттерн: https://refactoring.guru/ru/design-patterns/singleton\r\n  */\r\n\r\n  // Посмотрим на две реализаци старую - до ES6\r\n  oldSingletonDemo();\r\n  // И новую\r\n  newSingetonDemo();\r\n};\r\n\r\nconst oldSingletonDemo = () => {\r\n    // Смотрим реализацию в файле old-singleton.js\r\n\r\n    _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].add({id: 0, language: 'js'});\r\n    _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].add({id: 1, language: 'phyton'});\r\n    _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].add({id: 2, language: 'php'});\r\n    _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].add({id: 3, language: 'ruby'});\r\n\r\n    console.log('OldSingleton', _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"]);\r\n    let myLang = _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].get(0);\r\n    console.log('OldSingleton -> myLang', myLang);\r\n};\r\n\r\nconst newSingetonDemo = () => {\r\n  // Как и все в js в 2017-18 меньше, быстрее, чище!\r\n  _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].add({id: 0, language: 'js'});\r\n  _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].add({id: 1, language: 'phyton'});\r\n  _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].add({id: 2, language: 'php'});\r\n  _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].add({id: 3, language: 'ruby'});\r\n\r\n  console.log('NewSingleton', _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"]);\r\n  let myLang = _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].get(1);\r\n  console.log('NewSingleton -> myLang', myLang);\r\n\r\n  /*\r\n    Демо усложнить синглтон\r\n  */\r\n};\r\n\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (SingletonDemo);\r\n\n\n//# sourceURL=webpack:///./application/singleton/index.js?");

/***/ }),

/***/ "./application/singleton/new-singleton.js":
/*!************************************************!*\
  !*** ./application/singleton/new-singleton.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n\r\n// Так как является константой, не может быть измененно\r\nconst _data = [];\r\n\r\n// Создаем обьект и методы\r\nconst Store = {\r\n  add: item => _data.push(item),\r\n  get: id => _data.find( d=> d.id === id ),\r\n};\r\n// Замораживаем\r\nObject.freeze(Store);\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Store);\r\n\n\n//# sourceURL=webpack:///./application/singleton/new-singleton.js?");

/***/ }),

/***/ "./application/singleton/object_freeze.js":
/*!************************************************!*\
  !*** ./application/singleton/object_freeze.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst ObjectFreezeDemo = () => {\r\n  /*\r\n    Разберемся вначале с Object.freeze. ->\r\n    https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Object/freeze\r\n  */\r\n\r\n  let myObj = {\r\n    name: 'Dexter',\r\n    prop: () => {\r\n      console.log(`${undefined.name} woohoo!`);\r\n    }\r\n  };\r\n  console.log( myObj );\r\n  myObj.name = \"Debra\";\r\n  console.log( myObj );\r\n\r\n  /*\r\n    Заморозка обьекта, это необратимый процесс.\r\n    Единожды заомроженный обьект уже не может быть разморожен!\r\n    Заморозим обьект\r\n  */\r\n  let frozen = Object.freeze(myObj);\r\n  //Попробуем изменить или добавить значение\r\n  // frozen.name = \"Vince\";\r\n  // frozen.secondName = 'Morgan';\r\n\r\n  // Проверим сам обьект и его идентичность с тем,\r\n  // что мы создали в самом начале функции\r\n  // console.log( 'frozen', frozen, 'equal?', frozen === myObj);\r\n\r\n  /*\r\n    Так же, метод работает для массивов\r\n  */\r\n  let frozenArray = Object.freeze(['froze', 'inside', 'of', 'array']);\r\n\r\n  // Попробуем добавить новое значение\r\n  // frozenArray[0] = 'Noooooo!';\r\n\r\n  // Попробуем использовать методы\r\n  let sliceOfColdAndSadArray = frozenArray.slice(0, 1);\r\n  sliceOfColdAndSadArray.map( item => console.log( item ) );\r\n  console.log(frozenArray, sliceOfColdAndSadArray);\r\n\r\n  // Метоы для проверки\r\n  // Object.isFrozen( obj ) -> Вернет true если обьект заморожен\r\n  console.log(\r\n    'is myObj frozen?', Object.isFrozen( myObj ),\r\n    '\\nis frozen frozen?', Object.isFrozen( frozen ),\r\n    '\\nis array frozen', Object.isFrozen( frozenArray )\r\n  );\r\n\r\n  /*\r\n    Заморозка в обьектах является не глубокой\r\n  */\r\n\r\n  let universe = {\r\n    infinity: Infinity,\r\n    good: ['cats', 'love', 'rock-n-roll'],\r\n    evil: {\r\n      bonuses: ['cookies', 'great look']\r\n    }\r\n  };\r\n  let x = Object.getOwnPropertyNames(universe);\r\n  console.log(x);\r\n  // let frozenUniverse = Object.freeze(universe);\r\n      // frozenUniverse.humans = [];\r\n      // frozenUniverse.evil.humans = [];\r\n\r\n      // console.log(frozenUniverse);\r\n};\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (ObjectFreezeDemo);\r\n\n\n//# sourceURL=webpack:///./application/singleton/object_freeze.js?");

/***/ }),

/***/ "./application/singleton/old-singleton.js":
/*!************************************************!*\
  !*** ./application/singleton/old-singleton.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst OldSingleton = ( function(){\r\n var _data = [];\r\n\r\n function add(item){\r\n   _data.push(item);\r\n }\r\n\r\n function get(id){\r\n   return _data.find((d) => {\r\n       return d.id === id;\r\n   });\r\n }\r\n\r\n return {\r\n   add: add,\r\n   get: get\r\n };\r\n})();\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (OldSingleton);\r\n\n\n//# sourceURL=webpack:///./application/singleton/old-singleton.js?");

/***/ })

/******/ });
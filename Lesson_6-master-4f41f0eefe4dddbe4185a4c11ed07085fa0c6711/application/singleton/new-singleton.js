
// Так как является константой, не может быть измененно
const _data = [];

// Создаем обьект и методы
const Store = {
  add: item => _data.push(item),
  get: id => _data.find( d=> d.id === id ),
};
// Замораживаем
Object.freeze(Store);

export default Store;

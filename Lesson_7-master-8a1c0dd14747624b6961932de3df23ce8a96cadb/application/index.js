import Decorator from './decorator';
import AssignAndRes from './features/object.assign';
import Mediator from './mediator';

import Work1 from '../classworks/decorator_es7';

/*
  1. Decorator as Design Pattern -> ./decorator/index.js
  2. Higher Order Functions -> ./features/hoc.js
  3. Object.defineProperty(...) -> ./features/defineProperty
  4. Object.assign and Spread Operator
  5. ES7 Decorator -> ./decorator/es7_decorator

*/
  // Decorator();
  // AssignAndRes();
  Mediator();
  // classworks ->
  // Work1();

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/decorator/basicUsage.js":
/*!*********************************************!*\
  !*** ./application/decorator/basicUsage.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\nvar BaseDemo = function BaseDemo() {\n\n  console.log('DECORATOR BASE!');\n\n  function Human(name) {\n    this.name = name;\n    this.currentTemperature = 0;\n    this.minTemperature = -10;\n\n    console.log('new Human ' + this.name + ' arrived!');\n  }\n\n  Human.prototype.ChangeTemperature = function ChangeTemperature(changeValue) {\n    console.log('current', this.currentTemperature + changeValue, 'min', this.minTemperature);\n\n    this.currentTemperature = this.currentTemperature + changeValue;\n\n    if (this.currentTemperature < this.minTemperature) {\n      console.error('Temperature is to low: ' + this.currentTemperature + '. ' + this.name + ' died :(');\n    } else {\n      console.log('It\\'s cold outside (' + this.currentTemperature + ' deg), please wear some clothes, or ' + this.name + ' will die!');\n    }\n  };\n\n  var Morgan = new Human('Morgan');\n  Morgan.ChangeTemperature(-5);\n  Morgan.ChangeTemperature(-6);\n\n  function DressedHuman(Human) {\n    this.name = Human.name;\n    this.clothes = [{ name: 'jacket', temperatureResistance: 20 }, { name: 'hat', temperatureResistance: 5 }, { name: 'scarf', temperatureResistance: 10 }];\n    this.currentTemperature = 0;\n    this.minTemperature = Human.minTemperature - this.clothes.reduce(function (currentResistance, clothe) {\n      console.log('currentResistance', currentResistance, 'clothe', clothe);\n      return currentResistance + clothe.temperatureResistance;\n    }, 0);\n    console.log('new Human ' + this.name + ' arrived! He can survive in temperature ' + this.minTemperature);\n  }\n  DressedHuman.prototype = Human.prototype;\n\n  var Dexter = new DressedHuman(new Human('Dexter'));\n  Dexter.ChangeTemperature(-6);\n  Dexter.ChangeTemperature(-16);\n  Dexter.ChangeTemperature(-16);\n  Dexter.ChangeTemperature(-26);\n};\n\nexports.default = BaseDemo;\n\n//# sourceURL=webpack:///./application/decorator/basicUsage.js?");

/***/ }),

/***/ "./application/decorator/es7_decorator.js":
/*!************************************************!*\
  !*** ./application/decorator/es7_decorator.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {\n  var desc = {};\n  Object['ke' + 'ys'](descriptor).forEach(function (key) {\n    desc[key] = descriptor[key];\n  });\n  desc.enumerable = !!desc.enumerable;\n  desc.configurable = !!desc.configurable;\n\n  if ('value' in desc || desc.initializer) {\n    desc.writable = true;\n  }\n\n  desc = decorators.slice().reverse().reduce(function (desc, decorator) {\n    return decorator(target, property, desc) || desc;\n  }, desc);\n\n  if (context && desc.initializer !== void 0) {\n    desc.value = desc.initializer ? desc.initializer.call(context) : void 0;\n    desc.initializer = undefined;\n  }\n\n  if (desc.initializer === void 0) {\n    Object['define' + 'Property'](target, property, desc);\n    desc = null;\n  }\n\n  return desc;\n}\n\nvar FunctionDecorator = exports.FunctionDecorator = function FunctionDecorator() {\n\n  /*\n    ES6 Function Decorator:\n     1.\n  */\n\n  function fluent(fn) {\n    return function () {\n      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {\n        args[_key] = arguments[_key];\n      }\n\n      console.log('fluent args', args);\n      fn.apply(this, args);\n      return this;\n    };\n  }\n\n  function Person() {}\n  Person.prototype.setName = fluent(function (first, last) {\n    this.first = first;\n    this.last = last;\n  });\n\n  Person.prototype.sayName = function () {\n    console.log('Person name is', this.first, this.last);\n  };\n\n  var Peter = new Person();\n  Peter.setName('Peter', 'Jackson');\n  Peter.sayName();\n  console.log(Peter);\n\n  Peter.setName('Peter', 'Parker').sayName().setName('Bilbo', 'Bagins').sayName().sayName();\n};\n\nvar es7Decorator = exports.es7Decorator = function es7Decorator() {\n  var _desc, _value, _class;\n\n  /*\n    Декораторы принимают 3 аргумента:\n      - target : обьект к которому применяется декоратор\n      - key : название метода\n      - descriptor : дескриптор метода или свойства обьекта, это обьект который определяет\n      как эта функция должна работать.\n       Object.defineProperty();\n       Благодаря декоратору, мы можем применить, его к ф-и или обьекту и вернуть примененный дескриптор.\n      Новый дескриптор который мы возвращаем, станет дескриптором функции к которой мы хотим его применить.\n       Звучит немного не очевидно, и сразу не совсем понятно, что мы можем с этим делать.\n      На самом деле эта механика очень гибкая. Одно из свойств аргумента descriptor это\n      value, который является функцией которую мы хотим присвоить.\n      Мы можем перезаписать это новой функцией или в целом делать что угодно с этой функцией.\n  */\n\n  function decorate(target, key, descriptor) {\n    console.log('target', target, 'key', key, 'descriptor', descriptor);\n    var originFn = descriptor.value;\n\n    descriptor.value = function () {\n      for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {\n        args[_key2] = arguments[_key2];\n      }\n\n      originFn.apply(target, args);\n      return target;\n    };\n  }\n\n  var Person = (_class = function () {\n    function Person() {\n      _classCallCheck(this, Person);\n    }\n\n    _createClass(Person, [{\n      key: 'setName',\n      value: function setName(first, last) {\n        this.first = first;\n        this.last = last;\n      }\n    }, {\n      key: 'sayName',\n      value: function sayName() {\n        console.log('Person name is', this.first, this.last);\n      }\n    }]);\n\n    return Person;\n  }(), (_applyDecoratedDescriptor(_class.prototype, 'setName', [decorate], Object.getOwnPropertyDescriptor(_class.prototype, 'setName'), _class.prototype)), _class);\n\n\n  var Hobbit = new Person();\n  Hobbit.setName('Barliman', 'Butterbur');\n  Hobbit.sayName();\n\n  // Небольшой хак, для динамичного использования декораторов\n  function DecorateWith(decorator) {\n    return function (target, name, descriptor) {\n      descriptor.value = decorator.call(target, descriptor.value);\n    };\n  }\n\n  // - - - - - - - - - - - - - - - - - - - - - -\n\n  // let Arr = Array(100).fill(0).map( item => Math.floor(Math.random()*100) );\n  // console.log( Arr );\n  // console.log( '- - - - - - - - - - - -');\n\n\n  // function time( target, key, descriptor){\n  //   // Делаем bind на случай если в нашей исходной функии используется this\n  //   const originFn = descriptor.value.bind(this);\n  //\n  //   let index = 0;\n  //   descriptor.value = function ( ...args){\n  //     let id = index++;\n  //     console.time( key + id );\n  //     let value = originFn( ...args );\n  //     console.timeEnd( key + id);\n  //     return value;\n  //   }\n  // }\n  // const Obj = {\n  //   @time\n  //   squareAll( arr) {\n  //     return arr.map( x => x * x );\n  //   }\n  // }\n  // let x = Obj.squareAll( Arr )\n  // console.log( Arr, x);\n\n}; //end\n\n//# sourceURL=webpack:///./application/decorator/es7_decorator.js?");

/***/ }),

/***/ "./application/decorator/index.js":
/*!****************************************!*\
  !*** ./application/decorator/index.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\nvar _basicUsage = __webpack_require__(/*! ./basicUsage */ \"./application/decorator/basicUsage.js\");\n\nvar _basicUsage2 = _interopRequireDefault(_basicUsage);\n\nvar _es7_decorator = __webpack_require__(/*! ./es7_decorator */ \"./application/decorator/es7_decorator.js\");\n\nvar _hoc = __webpack_require__(/*! ../features/hoc */ \"./application/features/hoc.js\");\n\nvar _hoc2 = _interopRequireDefault(_hoc);\n\nvar _define_property = __webpack_require__(/*! ../features/define_property */ \"./application/features/define_property.js\");\n\nvar _define_property2 = _interopRequireDefault(_define_property);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\n/*\n  Декоратор — это структурный паттерн проектирования,\n  который позволяет динамически добавлять объектам новую\n  функциональность, оборачивая их в полезные «обёртки».\n\n  https://refactoring.guru/ru/design-patterns/decorator\n*/\n\nvar DecoratorDemo = function DecoratorDemo() {\n\n  // console.log( 'DECORATOR AS DESIGN PATTERN DEMO!');\n  // Base();\n  // console.log( '- - - - - - - - - - - -');\n  // Hoc();\n  // Define();\n  // console.log( '- - - - - - - - - - - -');\n  (0, _es7_decorator.FunctionDecorator)();\n\n  var Testo = function Testo() {\n    _classCallCheck(this, Testo);\n  };\n  // console.log( new Testo('Grecha') );\n\n  (0, _es7_decorator.es7Decorator)();\n};\n\nexports.default = DecoratorDemo;\n\n//# sourceURL=webpack:///./application/decorator/index.js?");

/***/ }),

/***/ "./application/features/define_property.js":
/*!*************************************************!*\
  !*** ./application/features/define_property.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\nvar ObjectDefineDemo = function ObjectDefineDemo() {\n\n  /*\n    Синтаксис:\n    Object.defineProperty(obj, prop, descriptor)\n     obj - Объект, в котором объявляется свойство.\n    prop - Имя свойства, которое нужно объявить или модифицировать.\n    descriptor - Дескриптор – объект, который описывает поведение свойства\n      В нём могут быть следующие поля:\n   */\n\n  var MyObj = {};\n  MyObj.title = \"Prop\";\n  MyObj.func = function () {\n    return this.name;\n  };\n  Object.defineProperty(MyObj, \"name\", {\n    // value – значение свойства, по умолчанию undefined\n    value: \"Вася\",\n    // configurable – если true, то свойство можно удалять, а также менять его\n    // в дальнейшем при помощи новых вызовов defineProperty. По умолчанию false.\n    configurable: true,\n    // writable – значение свойства можно менять, если true. По умолчанию false.\n    writable: true,\n    //enumerable – если true, то свойство просматривается в цикле for..in и\n    // методе Object.keys(). По умолчанию false.\n    enumerable: false\n    // get – функция, которая возвращает значение свойства. По умолчанию undefined.\n    // get: () => { console.log( 'getter'); },\n    // set – функция, которая записывает значение свойства. По умолчанию undefined.\n    // set: () => { console.log( 'setter'); }\n  });\n\n  //writable demo ->\n  MyObj.name = 10;\n\n  // configurable demo ->\n  // delete MyObj.name;\n\n  // enumerable demo ->\n  Object.defineProperty(MyObj, \"func\", { enumerable: false }); // Можно добавить уже после обьявления\n  for (var key in MyObj) {\n    console.log(key);\n  }console.log(Object.keys(MyObj));\n\n  // - - - - - - - - - - - - - -\n  Object.defineProperty(MyObj, \"fullName\", {\n    get: function get() {\n      return this.title + \" \" + this.name;\n    }\n  });\n\n  console.log('FullName getter', MyObj.fullName);\n\n  Object.defineProperty(MyObj, \"height\", {\n    get: function get() {\n      return this.ObjHeight + \" cm\";\n    },\n    set: function set(value) {\n      this.ObjHeight = value;\n      console.log('ObjHeight setter', this.ObjHeight);\n    }\n  });\n\n  MyObj.height = 20;\n  console.log(MyObj.height);\n  console.log(MyObj);\n};\n\nexports.default = ObjectDefineDemo;\n\n//# sourceURL=webpack:///./application/features/define_property.js?");

/***/ }),

/***/ "./application/features/hoc.js":
/*!*************************************!*\
  !*** ./application/features/hoc.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\n/*\n\n  Higher Order Functions\n  function is a values\n\n*/\nvar hofDemo = function hofDemo() {\n\n  var multiply = function multiply(x) {\n    return x * x;\n  };\n  var nine = multiply(3);\n\n  console.log('multiply:', nine);\n\n  /*\n     Array.filter (так же как map, forEach, etc...) пример использования HOF в нативном js\n    Паттерн позворялет использовать композцицию что бы собрать из маленьких функций одну большую\n   */\n\n  var zoo = [{ id: 0, name: \"WoofMaker\", species: 'dog' }, { id: 1, name: \"WhiteFurr\", species: 'rabbit' }, { id: 2, name: \"MeowMaker\", species: 'cat' }, { id: 3, name: \"PoopMaker\", species: 'dog' }, { id: 4, name: \"ScratchMaker\", species: 'cat' }];\n\n  var isDog = function isDog(animal) {\n    return animal.species === 'dog';\n  };\n  var isCat = function isCat(animal) {\n    return animal.species === 'cat';\n  };\n\n  var dogs = zoo.filter(isDog);\n  var cats = zoo.filter(isCat);\n\n  console.log('Here dogs:', dogs);\n  console.log('Here cats:', cats);\n\n  // - - - - - - - - - - - - - - - - - -\n\n  function compose(a, b) {\n    return function (c) {\n      return a(b(c));\n    };\n  }\n\n  var addTwo = function addTwo(value) {\n    return value + 2;\n  };\n  var multiplyTwo = function multiplyTwo(value) {\n    return value * 2;\n  };\n\n  var addTwoAndMultiplayTwo = compose(addTwo, multiplyTwo);\n  console.log(addTwoAndMultiplayTwo(2), addTwoAndMultiplayTwo(6), addTwoAndMultiplayTwo(40));\n};\n\nexports.default = hofDemo;\n\n//# sourceURL=webpack:///./application/features/hoc.js?");

/***/ }),

/***/ "./application/features/object.assign.js":
/*!***********************************************!*\
  !*** ./application/features/object.assign.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n/*\n  Object Assign, Spread and Rest Operator\n  Docs:\n    https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Object/assign\n*/\n\nvar SpreadOperatorDemo = function SpreadOperatorDemo() {\n\n  /*\n     String Literal:\n   */\n\n  // var a = 'Карп',\n  //     b = \"Селедка\";\n  // var newStringLiteral = `${a} и ${b} сидели на трубe`;\n  //     console.log('newStringLiteral', newStringLiteral);\n  //\n  //     var d = ['Лебедь','Рак','Щука'];\n  //     var c = `\n  //     <div>\n  //       <ul>\n  //         ${\n  //           d.map( (item) => `<li>${item}</li>` )\n  //         }\n  //       </ul>\n  //     </div>\n  //     `;\n  //\n  //     console.log(c);\n\n  /*\n    Object.assign\n    syntax: Object.assign(target, ...sources)\n  */\n  // не коментить DataObj\n  var DataObj = {\n    data1: 'data1',\n    data2: 'data2'\n  };\n\n  var DataObj2 = {\n    data3: 'data3',\n    data4: 'data4'\n  };\n\n  // - - - - - - - -\n\n  // let firstAssign = Object.assign(DataObj, DataObj2);\n  // console.log('firstAssign', firstAssign);\n  // console.log('DataObj', DataObj);\n  // Изменяем значение исходного обьекта и проверяем значения обеих\n  // DataObj.data5 = 'data5';\n  // console.log('DataObj', DataObj);\n  // console.log('firstAssign', firstAssign);\n\n  // - - - - - - - -\n\n  // IMMUTABLE ASSIGN\n  // let secondAssign = Object.assign({}, DataObj, DataObj2 );\n  // console.log( 'secondAssign', secondAssign );\n  // DataObj.data6 = 'data6';\n  // console.log('DataObj - secondAssign', DataObj);\n  // console.log('secondAssign', secondAssign);\n\n  // - - - - - - - -\n\n  // let FunctionalObj = {\n  //   x: () => {\n  //     console.log('some important stuff');\n  //   },\n  //   y: {\n  //     a: 'a',\n  //     b: 'b',\n  //     c: 'c'\n  //   }\n  // };\n  //\n  // FunctionalObj.x();\n  // let thirdAssign = Object.assign({}, FunctionalObj);\n  // console.log( thirdAssign );\n  // thirdAssign.x();\n\n  // - - - - - - - -\n  // convert to obj\n  // var v1 = 'abcfadfsdfsds';\n  // var v2 = true;\n  // var v3 = 10;\n  // var v4 = { value : true };\n  //\n  //\n  // var obj = Object.assign(\n  //   {},\n  //   v1,   // разберет посимвольно индекс-буква\n  //   null, // -> ignore\n  //   v2,   // -> ignore\n  //   undefined,  // -> ignore\n  //   v3,   // -> ignore\n  //   v4    // внесет в обьект\n  // );\n  // console.log(obj);\n\n  // - - - - - - - -\n\n  // var obj = {\n  //   foo: 1,\n  //   get bar() {\n  //     return 2;\n  //   }\n  // };\n\n  // obj.value = '';\n  // // При попытке скопировать значение с геттером, получим только его value\n  // var copy = Object.assign({}, obj);\n  // console.log(copy);\n\n\n  /*\n    REST and Spread Operator\n  */\n\n  // in Function ->\n  // function RestTest(a, b, ...props){\n  //   console.log('a:', a, 'b', b, 'props', props);\n  //   // props.map( item => console.log( 'map rest props', item ) )\n  // }\n  // var args = [0, 1, 2, 4, 5];\n  // RestTest.apply(null, args);\n  // RestTest(6,7,8,9,0,'f')\n\n\n  // In array:\n  // var iterableObj = ['i','t','e'];\n  // var iterableObj2 = ['d','d','a'];\n  // var x = [ '4', 'five', 6, ...iterableObj, ...iterableObj2];\n  // console.log( 'rest in array:', x);\n\n  // concat arrays: old way;\n  // var arr1 = [0, 1, 2];\n  // var arr2 = [3, 4, 5];\n  // Append all items from arr2 onto arr1\n  // arr1 = arr1.concat(arr2);\n  // new way:\n  // arr1 = [...arr1, ...arr2];\n  // console.log( arr1 );\n\n  // // In obj\n  // var obj = { value: 1};\n\n  // let objClone = { ...obj, ...DataObj };\n  // console.log( 'objClone', objClone );\n\n  // let { x, k, ...z } = { x: ['x'], k: ['k'], a: 3, b: 4 };\n  // console.log(x); // ['x']\n  // console.log(k); // ['k']\n  // console.log(z); // { a: 3, b: 4 }\n}; // end\n\nexports.default = SpreadOperatorDemo;\n\n//# sourceURL=webpack:///./application/features/object.assign.js?");

/***/ }),

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar _decorator = __webpack_require__(/*! ./decorator */ \"./application/decorator/index.js\");\n\nvar _decorator2 = _interopRequireDefault(_decorator);\n\nvar _object = __webpack_require__(/*! ./features/object.assign */ \"./application/features/object.assign.js\");\n\nvar _object2 = _interopRequireDefault(_object);\n\nvar _mediator = __webpack_require__(/*! ./mediator */ \"./application/mediator/index.js\");\n\nvar _mediator2 = _interopRequireDefault(_mediator);\n\nvar _decorator_es = __webpack_require__(/*! ../classworks/decorator_es7 */ \"./classworks/decorator_es7.js\");\n\nvar _decorator_es2 = _interopRequireDefault(_decorator_es);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n/*\n  1. Decorator as Design Pattern -> ./decorator/index.js\n  2. Higher Order Functions -> ./features/hoc.js\n  3. Object.defineProperty(...) -> ./features/defineProperty\n  4. Object.assign and Spread Operator\n  5. ES7 Decorator -> ./decorator/es7_decorator\n\n*/\n// Decorator();\n// AssignAndRes();\n(0, _mediator2.default)();\n// classworks ->\n// Work1();\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./application/mediator/index.js":
/*!***************************************!*\
  !*** ./application/mediator/index.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\nvar MediatorDemo = function MediatorDemo() {\n    function Daddy() {}\n    Daddy.prototype = {\n        getBeer: function getBeer() {\n            if (!kitchen.tryToGetBeer()) {\n                console.log(\"Daddy: Who the hell drank all my beer?\");\n                return false;\n            }\n            console.log(\"Daddy: Yeeah! My beer!\");\n            kitchen.oneBeerHasGone();\n            return true;\n        },\n        argue_back: function argue_back() {\n            console.log(\"Daddy: it's my last beer, for shure!\");\n        }\n    };\n\n    function Mammy() {}\n    Mammy.prototype = {\n        argue: function argue() {\n            console.log(\"Mammy: You are f*king alconaut!\");\n            kitchen.disputeStarted();\n        }\n    };\n    function BeerStorage(beer_bottle_count) {\n        this._beer_bottle_count = beer_bottle_count;\n    }\n    BeerStorage.prototype = {\n        takeOneBeerAway: function takeOneBeerAway() {\n            if (this._beer_bottle_count == 0) return false;\n            this._beer_bottle_count--;\n            return true;\n        }\n    };\n\n    var kitchen = {\n        daddy: new Daddy(),\n        mammy: new Mammy(),\n        refrigerator: new BeerStorage(3),\n        stash: new BeerStorage(2),\n\n        tryToGetBeer: function tryToGetBeer() {\n            if (this.refrigerator.takeOneBeerAway()) return true;\n            if (this.stash.takeOneBeerAway()) return true;\n\n            return false;\n        },\n        oneBeerHasGone: function oneBeerHasGone() {\n            this.mammy.argue();\n        },\n        disputeStarted: function disputeStarted() {\n            this.daddy.argue_back();\n        }\n    };\n    var round_counter = 0;\n    while (kitchen.daddy.getBeer()) {\n        round_counter++;\n        console.log(round_counter + \" round passed\");\n    }\n};\n\nexports.default = MediatorDemo;\n\n//# sourceURL=webpack:///./application/mediator/index.js?");

/***/ }),

/***/ "./classworks/decorator_es7.js":
/*!*************************************!*\
  !*** ./classworks/decorator_es7.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\n/*\n\n  Задание:\n    1. Используя функциональный декоратор, написать декоратор который будет показывать\n       аргументы и результат выполнения функции.\n\n    2. Написать декоратор для класса, который будет преобразовывать аргументы в число,\n       если они переданы строкой, и выводить ошибку если переданая переменная не\n       может быть преобразована в число\n*/\n\nvar Work1 = function Work1() {\n  var CoolMath = function () {\n    function CoolMath() {\n      _classCallCheck(this, CoolMath);\n    }\n\n    _createClass(CoolMath, [{\n      key: \"addNumbers\",\n      value: function addNumbers(a, b) {\n        return a + b;\n      }\n    }, {\n      key: \"multiplyNumbers\",\n      value: function multiplyNumbers(a, b) {\n        return a * b;\n      }\n    }, {\n      key: \"minusNumbers\",\n      value: function minusNumbers(a, b) {\n        return a - b;\n      }\n    }]);\n\n    return CoolMath;\n  }();\n\n  var Calcul = new CoolMath();\n  var x = Calcul.addNumbers(2, 2);\n  var y = Calcul.multiplyNumbers(10, 2);\n  var z = Calcul.minusNumbers(10, 2);\n  console.log(x, y, z);\n};\n\nexports.default = Work1;\n\n//# sourceURL=webpack:///./classworks/decorator_es7.js?");

/***/ })

/******/ });
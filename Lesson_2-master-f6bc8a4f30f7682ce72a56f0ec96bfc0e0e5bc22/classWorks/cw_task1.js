/*

  Данные: http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
  Задача.

  1.  Получить данные и в виде простой таблички вывести список компаний. Для начала используем поля:
      Company | Balance | Registered | Показать адресс | Кол-во employers | показать сотрудников

  2.  Сделать сортировку таблицы по количеству сотрудников и балансу. Сортировка должна происходить по клику
      на заголовок столбца

  3.  По клику на показать адресс должна собиратся строка из полей адресса и показываться на экран.

  4.  По клику на показать сотрудников должна показываться другая табличка формата:

      <- Назад к списку компаний | *Название компании*
      Сотрудники:
      Name | Gender | Age | Contacts

  5.  В второй табличке долен быть реализован поиск сотрудников по их имени, а так же сортировка по
      полу и возрасту.

  Примечание: Весь код должен писатся с учетом синтаксиса и возмжность ES6.

*/

fetch('http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2').then(
  function ( responce ) {
    return responce.json();
  }).then(ConstructTable);

function ConstructTable ( data ){
  console.log('Construct', data);
  
  data.map( ( item, key ) => {

    new CreateRowData(item);
    // console.log(item.company);
    // console.log(item.balance);
    // console.log(item.registered);

    let row_1 = document.createElement("tr");
    let td_num = document.createElement("td");
    let td_com = document.createElement("td");
    let td_bal= document.createElement("td");
    let td_reg = document.createElement("td");
    let td_emp = document.createElement("td");
    let td_show_adr = document.createElement("td"); 
    let td_show_emp = document.createElement("td");
      
    let button_address = document.createElement("button");
    button_address.innerText = "Показать адресс";
    td_show_adr.appendChild(button_address);

    button_address.addEventListener("click", function () {
      td_show_adr.removeChild(button_address);
      td_show_adr.innerText = item.address.city + ", " + item.address.zip + ", " + item.address.country + ", " + item.address.state + ", " + item.address.street + ", " + item.address.house;
    });

    let button_employers = document.createElement("button");
    button_employers.innerText = "Показать сотрудников";
    td_show_emp.appendChild(button_employers);

    button_employers.addEventListener("click", function () {
      // td_show_emp.removeChild(button_employers);
      // document.body.removeChild(main_table);

      // let table_2 = document.createElement("table");
      // table_2.id = "second_table";
      // console.log(table_2);

      // data.map( ( item, key ) => {

      //   let a = new CreateRowData2(item);
      //   console.log(a);

      //   table_2.id = "second_table";
      //   let row_2 = document.createElement("tr");
      //   let td_name = document.createElement("td");
      //   let td_gender = document.createElement("td");
      //   let td_age = document.createElement("td");
      //   let td_contacts = document.createElement("td");

      //   td_name.classList.add("block_num");
      //   td_gender.classList.add("block_com");
      //   td_age.classList.add("block_bal");
      //   td_contacts.classList.add("block_reg");

      //   row_2.appendChild(td_name);
      //   row_2.appendChild(td_gender);
      //   row_2.appendChild(td_age);
      //   row_2.appendChild(td_contacts);

      //   let second_table = document.getElementById("second_table");

      //   second_table.appendChild(row_2);

      main_table.classList.add("invincible");
      let second_table = document.createElement("table");
      second_table.id = "second_table";
      new CreateRowData2(item);

      data.map( ( item, key ) => {

        let row_2 = document.createElement("tr");
        let td_name = document.createElement("td");
        let td_gender = document.createElement("td");
        let td_age = document.createElement("td");
        let td_contacts = document.createElement("td");

        td_name.classList.add("block_num");
        td_gender.classList.add("block_com");
        td_age.classList.add("block_bal");
        td_contacts.classList.add("block_reg");

        console.log();

        td_name.innerText = item.name;
        td_gender.innerText = item.gender;
        td_age.innerText = item.age;
        td_contacts.innerText = item.contacts;

        // console.log(td_name, td_gender, td_age, td_contacts);

        row_2.appendChild(td_name);
        row_2.appendChild(td_gender);
        row_2.appendChild(td_age);
        row_2.appendChild(td_contacts);

        second_table.appendChild(row_2);

      })
      
     

      
    });

    td_num.classList.add("block_num");
    td_com.classList.add("block_com");
    td_bal.classList.add("block_bal");
    td_reg.classList.add("block_reg");
    td_emp.classList.add("block_emp");
    td_show_adr.classList.add("block_show_adr");
    td_show_emp.classList.add("block_show_emp");

    td_num.innerText = key;
    td_com.innerText = item.company;
    td_bal.innerText = item.balance;
    td_reg.innerText = item.registered;
    td_emp.innerText = item.employers.length - 1;




    row_1.appendChild(td_num);
    row_1.appendChild(td_com);
    row_1.appendChild(td_bal);
    row_1.appendChild(td_reg);
    row_1.appendChild(td_emp);
    row_1.appendChild(td_show_adr);
    row_1.appendChild(td_show_emp);

    main_table.appendChild(row_1);

    




  })
}

function CreateRowData ( companyArray ){
  let{address, balance, company, employers, registered} = companyArray;
  this.company = company;
  this.balance = balance;
  this.registered = registered;
  this.employers = employers;
  return this;
}

function CreateRowData2 ( companyArray ){
  let{employers} = companyArray;
  this.employers = employers;
  return this;
}

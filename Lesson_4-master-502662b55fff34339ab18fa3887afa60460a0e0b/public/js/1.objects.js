/*

  Objects in JS

*/

//
// var o = new Object();
//     o.temp = "123";
// var o = {}; // пустые фигурные скобки
// console.log('12312313');

// let cat = {
//   sound: 'meow',
//   talk: function(){
//     console.log( this.sound );
//   }
// };
//
// let fox = { sound: 'Hati Ho' };
// cat.talk();
//
// window.sound = 'brbrbrbrbr';
//
// let talkFunction = cat.talk;
//     talkFunction();


// let bindFunction = talkFunction;
//     bindFunction();
//


// let meowButton = document.getElementById('meowButton');
//     console.log( meowButton );
//     meowButton.addEventListener(
//       'click',
//       function( e ){
//         console.log( e );
//       }
//     );

  // function testThis(){
  //   // "use strict";
  //   return this;
  // }
  // console.log( testThis() );

  // cat.walk = function(){
  //   console.log('cat walk and say ' + this.sound);
  // };
  //
  // console.log( cat );
  // console.log( cat.walk );


  // function talk(){
  //   console.log( this.sound );
  // }
  //
  // let boromir = {
  //   speak: talk,
  //   sound: "Нельзя просто так взять и..."
  // };
  //
  // boromir.speak();
  //
  // let gollum = {
  //    blab: boromir.speak,
  //    sound: "Моя прелесть...."
  // };
  //
  // gollum.blab();

  /* 3 */
  // var Human = {
  //   name: 'Ivan',
  //   sayName: function(){
  //     console.log( ' my name is ' + this.name );
  //   },
  //   personTwo: {
  //     name: 'Petro',
  //     sayName: function(){
  //       console.log( ' my name is ' + this.name );
  //     }
  //   }
  // };
  // //
  // Human.sayName();
  // Human.personTwo.sayName();


  // Функция конструктор обьекта
  // function Student( name, profession ){
  //   this.name = name;
  //   this.profession = profession;
  //   this.sayName = function(){
  //     console.log('my name:', this.name );
  //   };
  // }
  // //
  // var Dima = new Student( 'Dima', 'Frontend');
  //     Dima.sayName();


    function Car(model, hp, maxspeed, price) {
      this.model = model;
      this.hp = hp;
      this.maxspeed = maxspeed;
      this.price = price;
      this.ifPrice = function (price){
        if (this.price < 10000) {
          return "small -"
        }
        else {
          return "big -"
        }
      }
      this.showPrice = function (){
        console.log("Prise is:", this.ifPrice(), this.price);
      }
    }

    var myCar = new Car("bmw", 200, 250, 60000);
        myCar.showPrice();

     /*

        1. Создать ф-ю констурктор которая создаст новый обьект вашего типа
        2. Обьект должен иметь пару свойств
        3. Функцию которая производит манипуляцию со свойствами
        4. Функция которая перебором выводит все свойства

      */
      // let animal = {
      //   sound: 'asdasd'
      // };
      // function dosmsng(){
      //   console.log( this.sound );
      // }
      //
      // let meowButton = document.getElementById('meowButton');
      //     console.log( meowButton );
      //     meowButton.addEventListener(
      //       'click',
      //       ClickSound.bind(animal)
      //     );


  /*
    call, bind, apply

    Apply:
    fun.apply(thisArg, [argsArray])

  */
  //
  // function add(c, d, l) {
  //   console.log(this);
  //   console.log(this.a + this.b + c + d + this.z + this.o);
  // }
  // add(3,4); //NaN
  // var ten = {a: 1, b: 2, z:5, o: 6, rand: function(){ return 1; }};
  // add.call(ten, 3, 4, 9); // 10
  // var x = [1,2,3,6];
  // add.apply(ten, x);

    // function Product(name, price) {
    //   this.name = name;
    //   this.price = price;
    //
    //   if (price < 0) {
    //     throw RangeError('Cannot create product ' +
    //                       this.name + ' with a negative price');
    //   }
    // }
    //
    // function Food(name, price) {
    //   Product.call(this, name, price);
    //   this.category = 'food';
    // }
    //
    // function Toy(name, price) {
    //   Product.call(this, name, price);
    //   this.category = 'toy';
    // }
    //
    // var cheese = new Food('feta', 5);
    // var fun = new Toy('robot', 40);
    // console.log( fun, cheese );
